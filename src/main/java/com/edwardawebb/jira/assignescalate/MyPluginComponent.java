package com.edwardawebb.jira.assignescalate;

public interface MyPluginComponent
{
    String getName();
}